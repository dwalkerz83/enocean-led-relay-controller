#include <EnOceanMsg.h>

//Libraries definitions
//Timer library
//EnOcean library
#include <EnOceanMsg.h>

//Definition of the pin used in program
const int _OutPinSalon = 13;
const int _OutPinCockpit = 12;


//Global variables
EnOceanMsg _Msg;
bool _TimerExpire = false;
int _NbClick = 0;



void TurnOnSalon() 
{
analogWrite(_OutPinSalon, 255);
}
void TurnOnCockpit()
{
analogWrite(_OutPinCockpit, 255);
}
void TurnOffSalon()
{
  analogWrite(_OutPinSalon, 0);
}
void TurnOffCockpit()
{
  analogWrite(_OutPinCockpit, 0);
}

void setup() 
{
  //USB
  Serial.begin(9600);
  //Pin 0/1
  Serial1.begin(57600);
  //Pin 18/19
  
  //pinMode(_InPinIrDetector,INPUT);
  //pinMode(_InPinFireDetection,INPUT);
  pinMode(_OutPinSalon, OUTPUT);
  pinMode(_OutPinCockpit, OUTPUT);

  
  digitalWrite(_OutPinSalon, LOW);
  digitalWrite(_OutPinCockpit, LOW);
 
  

}

void actionTaker()
{
  if (_Msg.getPayload() == 0x10)
  {
    Serial.println("The user push the button B0/0.");
    TurnOnSalon();
    _NbClick=0;
  }
  else if (_Msg.getPayload() == 0x30)
  {
    Serial.println("The user push the button B0/1");
     TurnOffSalon();
     _NbClick=0;
  } 
   if (_Msg.getPayload() == 0x50)
   { Serial.println("The user push the button A0/0.");
    TurnOnCockpit();
    _NbClick=0;
  }
   if (_Msg.getPayload() == 0x80)
  {
    Serial.println("The user push the button A0/1");
     TurnOffCockpit();
     _NbClick=0;
  } 
  else
  {
    Serial.print("Received an unsupported command : ");
    Serial.println(_Msg.getPayload());
  }
}

// continuously reads packets, looking for ZB Receive or Modem Status
void loop() 
{
  delay(50); 
  _Msg.decode();
  if (_Msg.dataAvailable() == true)
  {
    actionTaker();
    _Msg.reset();
  }
}